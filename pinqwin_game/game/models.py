from django.db import models
from django.conf import settings

# Create your models here.

class GameTable(models.Model):
    game_title = models.CharField(max_length=32)
    last_state = models.TextField(max_length=2000)
    history = models.TextField()
    player1 = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="games_played")
    player2 = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="games_played_as_player2")
    winner = models.IntegerField()
