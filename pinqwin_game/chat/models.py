from django.db import models
from django.conf import settings

# Create your models here.

class Messages(models.Model):
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name="sent_messages")
    receiver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name="received_messages")
    content = models.TextField(max_length=280)
    sent_time = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField()
