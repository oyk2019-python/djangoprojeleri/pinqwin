"""pinqwin_game URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('users.urls')),
    path('', TemplateView.as_view(template_name="pinqwin_game/home.html"), name="home"),
    path('logged_out/', TemplateView.as_view(template_name="registration/logged_out.html"), name="logged_out"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(next_page="logged_out"), name="logout")
]

if settings.DEBUG:
    urlpatterns.append(re_path(r'^media/(?P<path>.*)$',
                               serve,
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}))
