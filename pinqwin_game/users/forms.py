"""from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm

class UserCreateForm(UserCreationForm):
    class Meta:
        fields = ("username", "email","password1", "password2")
        model = get_user_model()

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.fields["username"].label = "Display Name"
        self.fields["email"].label = "Email Address"
        """
from django import forms

from .models import UserTable
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.admin import UserAdmin

'''
class RegisterForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if (password and password2) and (password != password2):
            raise forms.ValidationError('Passwords does not match!')
        elif UserTable.objects.filter(username=username).exists():
            raise forms.ValidationError('This username exist in system. Please try different username.')
        elif UserTable.objects.filter(email=email).exists():
            raise forms.ValidationError('This email exist in system. Please try different email')
        else:

            values = {
                'username': username, 'email': email, 'password': password, 'password2': password2,
            }

            return values
'''
class NewUserCreation(UserCreationForm, forms.ModelForm):
    email = forms.EmailField(help_text='Email adresini giriniz!')

    class Meta(UserCreationForm.Meta):
        model = UserTable
        fields = UserCreationForm.Meta.fields + ('email',)



class ProfileUpdateForm(UserChangeForm, forms.ModelForm):
    #passsword1 = forms.CharField(widget=forms.PasswordInput)
    #passsword2 = forms.CharField(widget=forms.PasswordInput)

    class Meta(UserTable.Meta):
        model = UserTable
        fields = ['username', 'email', 'password']

    '''username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if (password and password2) and (password != password2):
            raise forms.ValidationError('Passwords does not match!')
        elif UserTable.objects.filter(username=username).exists():
            raise forms.ValidationError('This username exist in system. Please try different username.')
        elif UserTable.objects.filter(email=email).exists():
            raise forms.ValidationError('This email exist in system. Please try different email')
        else:
            values = {
                'username': username, 'email': email, 'password': password, 'password2': password2,
            }

            return values'''