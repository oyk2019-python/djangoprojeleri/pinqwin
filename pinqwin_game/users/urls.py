from django.urls import path
from users import views
from .views import UserDetailView, ProfileUpdateView
from django.contrib.auth import views as auth_views

urlpatterns = [
    #path("login/", auth_views.LoginView.as_view(template_name="accounts/login.html"),name='login'),
    #path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("sign_up/", views.RegisterCBS.as_view(), name="signup"),
    path('profile/', ProfileUpdateView.as_view(), name='update_profile'),
    path('detail/me/', UserDetailView.as_view(is_me=True), name='me'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
]


