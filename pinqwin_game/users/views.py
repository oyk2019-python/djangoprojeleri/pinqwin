'''from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView
from users import forms
# Create your views here.
from django.shortcuts import render
from .forms import UserCreationForm
from django.http import HttpResponse,HttpResponseRedirect
from .models import UserTable
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import reverse,redirect
from django.contrib.auth import login


class SignUp(CreateView):
    form_class = forms.UserCreateForm
    success_url = reverse_lazy('home')
    template_name = "users/sign_up.html"

from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from .forms import RegisterForm
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import reverse, redirect
from django.contrib.auth import login

# Create your views here.
from django.contrib.auth.forms import UserCreationForm

def register(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = UserTable.objects.create_user(username=username, email=email, password=password)
        login(request, user)
        return HttpResponse('Basarili kayit')
    else:
        return render(request, 'users/sign_in.html', {'form': form})
'''
from django.urls import reverse_lazy
from .models import UserTable
from django import forms
from django.views.generic import UpdateView, CreateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
#from .mixins import UserOwnedMixin
from .forms import NewUserCreation, ProfileUpdateForm


class RegisterCBS(CreateView):
    model = UserTable
    form_class = NewUserCreation

    success_url = '/admin'
    template_name = 'users/sign_up.html'

class UserDetailView(LoginRequiredMixin, DetailView):
    model = UserTable
    context_object_name = 'user_detail'
    is_me = False
    template_name = "users/usertable_form.html"

    def get_object(self, queryset=None):
        if self.is_me:
            return self.request.user
        else:
            return super().get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_me"] = self.is_me
        return context



class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = UserTable
    form_class = ProfileUpdateForm
    template_name = "users/profile_update.html"

    def get_success_url(self):
        return reverse_lazy("me")

    def get_object(self, queryset=None):
        return self.request.user

