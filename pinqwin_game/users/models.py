from django.db import models
from django.contrib.auth.models import User, AbstractUser

# Create your models here.

class UserTable(AbstractUser):
    is_registered = models.CharField(max_length=11, null=True, blank=True)
    win_streak = models.CharField(max_length=11, null=True, blank=True)

    def __str__(self):
        return self.username
